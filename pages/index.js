import Head from "next/head";
import Image from "next/image";
import HeaderV0, { HeaderV1 } from "/components/header";
import HeroV0 from "/components/hero";
import FooterV0 from "/components/footer";

export default function Home() {
  const portfolios = [
    {
      tag: "Blog",
      title: "Content Writing",
      description: (
        <span>
          Writing content about <br /> Mental Health Awareness
        </span>
      ),
      image: "/azla/4.png",
      link: "https://medium.com/@miss.azla",
    },

    {
      tag: "Arts",
      title: "Designing",
      description: (
        <span>
          Designing and modify <br /> diary, calendar, wallpaper
        </span>
      ),
      image: "/calendar.png",
      link: "https://www.pinterest.com/Azlasorubou/",
    },

    {
      tag: "Video",
      title: "Vlogging",
      description: (
        <span>
          Make traveling videos <br /> and others
        </span>
      ),
      image: "/vlog/vlog_journey.png",
      link: "https://azmo.page/all-vlog",
    },
  ];

  const aboutMe =
    "I'm the founder and content creator of Azmo.page. I write on health awareness based on my studies, personal experiences, and stories. I also produce videos when traveling, draw cartoons, and design strange things in addition to writing. ";
  const header = "My Bible Quote";
  const description =
    "Ask, and it shall be given you, seek, and ye shall find, knock, and it shall be opened unto you. for every one that asked, received; and he that seethe, found; and to him that knocked, it shall be opened. ~Matthew 7:7-8";
  return (
    <>
      <div className="font-sans subpixel-antialiased bg-rose-50">
        <Head>
          <title>Azla Sorubou | Portfolio</title>
          <meta
            name="description"
            content="This page gives a summary of what Azla has been up to."
          />
          <link rel="icon" href="/logo.png" />

          <meta property="og:title" content="Azla Sorubou | Portfolio" />
          <meta
            property="description"
            content="This page gives a summary of what Azla has been up to."
          />
          <meta
            property="og:description"
            content="This page gives a summary of what Azla has been up to."
          />
          <meta property="twitter:title" content="Welcome to Azmo" />
          <meta
            property="twitter:description"
            content="This page gives a summary of what Azla has been up to."
          />

          <meta property="og:image" content="/azla.png" />
          <meta property="twitter:image" content="/azla.png" />
        </Head>
        <HeaderV1 />
        <HeroV0 />
        <div className="overflow-x-hidden">
          <div className="relative mx-2 sm:mx-0 bg-stone-500 rounded-tl-[80px]  rounded-tr-[80px] md:rounded-tl-[150px] md:rounded-tr-[0px] text-rose-50 sm:translate-x-4 xl:translate-x-20">
            <div className="p-4 sm:p-8 mx-auto " id="about">
              <div className="m-4 p-2 sm:p-4 lg:p-10 xl:p-20 xl:w-2/3">
                <h2 className="font-bold text-xl sm:text-4xl">Who am I?</h2>
                <div className="mb-1">
                  <span className="inline-block w-40 h-1 rounded-full bg-rose-50"></span>
                  <span className="inline-block w-3 h-1 ml-1 rounded-full bg-rose-50"></span>
                  <span className="inline-block w-1 h-1 ml-1 rounded-full bg-rose-50"></span>
                </div>

                <p className="py-2 text-md sm:text-xl leading-loose max-w-prose">
                  {" "}
                  {aboutMe}{" "}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-stone-500 overflow-x-hidden">
        <div className="relative mx-2 sm:mx-0 bg-rose-50 rounded-bl-[80px]  rounded-br-[80px] md:rounded-bl-[150px] md:rounded-br-[0px] text-stone-600 sm:translate-x-4 xl:translate-x-20">
          <div className="p-4 sm:p-8 mx-auto">
            <div className="m-4 p-2 sm:p-4 lg:p-10 xl:p-20 xl:w-2/3">
              <h2 className="font-bold text-xl sm:text-4xl">{header}</h2>
              <div className="mb-1">
                <span className="inline-block w-40 h-1 rounded-full bg-stone-500"></span>
                <span className="inline-block w-3 h-1 ml-1 rounded-full bg-stone-500"></span>
                <span className="inline-block w-1 h-1 ml-1 rounded-full bg-stone-500"></span>
              </div>
              <p className="text-stone-600 py-2 text-md sm:text-xl leading-loose max-w-prose">
                {description}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-stone-500 pt-10" id="portfolio">
        <div className="relative">
          <div className="absolute inset-0 hp-rpc"> </div>
          <div className="hidden sm:block absolute top-0 left-0 w-12 bg-rose-50 h-16 rounded-tr-full rounded-br-full"></div>
          <div className="mx-2 sm:mx-10 bg-rose-50  rounded-tl-[80px] rounded-tr-[80px] md:rounded-tl-[150px] md:rounded-tr-[150px] hp-rpc">
            <div className="p-4 sm:p-8 mx-auto relative">
              <div className="absolute translate-x-40 xl:translate-x-80 translate-y-20 top-0 left-0">
                <span className="relative inline-block">
                  <svg
                    viewBox="0 0 52 24"
                    fill="currentColor"
                    className="absolute top-0 left-0 z-0 hidden w-32 -mt-8 -ml-20 text-stone-400 lg:w-32 lg:-ml-28 lg:-mt-10 sm:block"
                  >
                    <defs>
                      <pattern
                        id="679d5905-e08c-4b91-a66c-84aefbb9d2f5"
                        x="0"
                        y="0"
                        width=".135"
                        height=".30"
                      >
                        <circle cx="1" cy="1" r=".7" />
                      </pattern>
                    </defs>
                    <rect
                      fill="url(#679d5905-e08c-4b91-a66c-84aefbb9d2f5)"
                      width="52"
                      height="24"
                    />
                  </svg>
                </span>{" "}
              </div>

              <div className="absolute bottom-0 right-0 -translate-y-4 -translate-x-40">
                <span className="relative inline-block">
                  <svg
                    viewBox="0 0 52 24"
                    fill="currentColor"
                    className="absolute top-0 left-0 z-0 hidden w-32 -mt-8 -ml-20 text-stone-400 lg:w-32 lg:-ml-28 lg:-mt-10 sm:block"
                  >
                    <defs>
                      <pattern
                        id="679d5905-e08c-4b91-a66c-84aefbb9d2f5"
                        x="0"
                        y="0"
                        width=".135"
                        height=".30"
                      >
                        <circle cx="1" cy="1" r=".7" />
                      </pattern>
                    </defs>
                    <rect
                      fill="url(#679d5905-e08c-4b91-a66c-84aefbb9d2f5)"
                      width="52"
                      height="24"
                    />
                  </svg>
                </span>{" "}
              </div>

              <div className="m-4 p-2 sm:p-4 lg:p-10 xl:p-20 text-stone-600 text-center flex flex-col xl:flex-row items-center  justify-center gap-4">
                {portfolios.map((item, id) => (
                  <a href={item.link} key={id} target="blank">
                    <div className="w-96 hover:bg-stone-500 group rounded-xl hover:cursor-pointer">
                      <div className="h-12 w-1 bg-stone-500 mx-auto m-2 rounded-full group-hover:bg-rose-50" />
                      <div className="bg-stone-400 rounded-full w-24 p-1 text-stone-50 mx-auto my-2">
                        {item.tag}
                      </div>
                      <h2 className="font-bold text-xl sm:text-4xl group-hover:text-rose-50">
                        {item.title}
                      </h2>
                      <p className="text-stone-600 text-md sm:text-xl max-w-prose p-2 lg:p-4 group-hover:text-rose-50">
                        {item.description}
                      </p>
                      <img
                        src={item.image}
                        className="h-80 mx-auto rounded-xl w-80"
                      />
                      <p>. . .</p>
                    </div>
                  </a>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-rose-50 border-t hp-rpc">
        <FooterV0 />
      </div>
    </>
  );
}
