import { useState } from "react";

export default function HeaderV0() {
  const links = [
    { text: "Who am I", link: "#about" },
    { text: "Portfolio", link: "#portfolio" },
    { text: "Contact", link: "#contact" },
  ];
  return (
    <>
      <div className="p-2 md:p-4 flex flex-row justify-between bg-rose-50 hp-rpc">
        <div>
          <h1> AZLA SORUBOU </h1>
        </div>
        <div className="flex flex-row gap-6">
          {links.map((item, id) => (
            <a href={item.link} key={id}>
              {item.text}
            </a>
          ))}
        </div>
      </div>
    </>
  );
}

export function HeaderV1() {
  const title = "Azla";
  const [menu, setMenu] = useState(false);
  const links = [
    { text: "Who am I", link: "#about" },
    { text: "Portfolio", link: "#portfolio" },
    { text: "Get in touch", link: "#contact" },
  ];

  return (
    <nav className="border-b text-stone-800 hp-rpc fixed top-0 inset-x-0 z-20 bg-rose-50">
      <div className="px-6 py-4 mx-2 md:flex md:justify-between md:items-center">
        <div className="flex items-center justify-between">
          <div>
            <a
              className="text-2xl font-bold text-stone-600 lg:text-3xl hover:text-stone-700"
              href="#"
            >
              {title}
            </a>
          </div>

          <div className={`md:hidden `}>
            <button
              type="button"
              className="text-stone-500 hover:text-stone-600 focus:outline-none focus:text-stone-600 "
              aria-label="Toggle menu"
              onClick={() => setMenu(!menu)}
            >
              {!menu ? (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M4 6h16M4 12h16m-7 6h7"
                  />
                </svg>
              ) : (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M5 15l7-7 7 7"
                  />
                </svg>
              )}
            </button>
          </div>
        </div>

        <div className={`items-center md:flex ${menu ? "block" : "hidden"} `}>
          <div className="flex flex-col mt-4 space-y-4 md:flex-row md:items-center md:mt-0 md:space-y-0 md:-px-8">
            {links.map((item, id) => (
              <a
                className="block md:mx-4 font-medium text-stone-700 hover:text-stone-900 hover:underline hover:animate-pulse duration-200"
                href={item.link}
                key={id}
              >
                {item.text}
              </a>
            ))}
          </div>
        </div>
      </div>
    </nav>
  );
}
