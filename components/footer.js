import { useRef } from "react";
import emailjs from "@emailjs/browser";

console.log(process.env);
const [service, temp, user] = [
  process.env.NEXT_PUBLIC_EJS_SERVICE_ID,
  process.env.NEXT_PUBLIC_EJS_TEMP_ID,
  process.env.NEXT_PUBLIC_EJS_USER_ID,
];

export default function FooterV0() {
  const form = useRef();
  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm(service, temp, form.current, user).then(
      (result) => {
        console.log(result.text);
      },
      (error) => {
        console.log(error.text);
      }
    );
    alert("Your message has been sent!");
  };

  return (
    <>
      <div className="bg-stone-500 rounded-bl-xl rounded-br-xl sm:rounded-br-[150px] sm:rounded-bl-[150px]  mx-2 sm:mx-10">
        <div className="p-4 sm:p-8 mx-auto text-rose-50" id="contact">
          <div className="m-4 p-2 sm:p-4 lg:p-10 xl:p-20 xl:w-2/3 mx-auto">
            <h2 className="font-bold text-xl sm:text-4xl">Get in touch</h2>
            <div className="mb-1">
              <span className="inline-block w-40 h-1 rounded-full bg-rose-50"></span>
              <span className="inline-block w-3 h-1 ml-1 rounded-full bg-rose-50"></span>
              <span className="inline-block w-1 h-1 ml-1 rounded-full bg-rose-50"></span>
            </div>

            <p className="py-2 text-md sm:text-xl leading-loose max-w-prose"></p>
            <form className="mt-6" ref={form} onSubmit={sendEmail}>
              <div className="items-center -mx-2 md:flex">
                <div className="w-full mx-2">
                  <label className="block mb-2 text-sm font-medium text-rose-50">
                    Name
                  </label>
                  <input
                    className="block w-full px-4 py-4 text-stone-600 bg-white border rounded-md focus:border-stone-400 focus:ring-stone-300 focus:outline-none focus:ring focus:ring-opacity-40"
                    placeholder="John Doe"
                    name="user_name"
                    type="text"
                  />
                </div>

                <div className="w-full mx-2 mt-4 md:mt-0">
                  <label className="block mb-2 text-sm font-medium text-rose-50 ">
                    E-mail
                  </label>
                  <input
                    placeholder="johndoe@gmail.com"
                    name="user_email"
                    className="block w-full px-4 py-4 text-stone-600 bg-white border rounded-md  focus:border-stone-400 focus:ring-stone-300 focus:outline-none focus:ring focus:ring-opacity-40"
                    type="email"
                  />
                </div>
              </div>

              <div className="w-full mt-4">
                <label className="block mb-2 text-sm font-medium text-rose-50 ">
                  Message
                </label>

                <textarea
                  className="block w-full h-52 px-4 py-2 text-stone-600 bg-white border rounded-md focus:border-stone-400  focus:outline-none focus:ring focus:ring-stone-300 focus:ring-opacity-40"
                  name="message"
                  placeholder="Write your message here."
                ></textarea>
              </div>

              <div className="flex justify-end mt-6">
                <input
                  type="submit"
                  className="px-4 py-2 text-stone-600 transition-colors duration-200 transform bg-rose-50 rounded-md hover:bg-rose-100 focus:outline-none"
                  value="Send Message"
                />
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="p-4 text-center text-stone-500">
        <p> All rights reserved</p>
      </div>
    </>
  );
}
